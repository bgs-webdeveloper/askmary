$(document).ready(function () {
  var data = []
  var $elCards =  $('.card-select');
  for (var i = 0; i < $elCards.length; i++) {
    var price = $($elCards[i]).attr('data-price') && $($elCards[i]).attr('data-price').split(',')
    var weight = $($elCards[i]).attr('data-weight') && $($elCards[i]).attr('data-weight').split(',')

    if(!price || !weight) {
      data.push([])
    } else {
      var options = []
      var maxLength = Math.max(price.length, weight.length)

      for (var j = 0; j < maxLength; j++) {
        var indexString = i.toString() + j.toString();
        options.push({
          id: indexString,
          text: '<div class="select-info"><span class="select-info__cost">' + price[j] + '</span><span class="select-info__value">' + weight[j] + '</span></div>',
          html: '<div class="select-info"><span class="select-info__cost">' + price[j] + '</span><span class="select-info__value">' + weight[j] + '</span></div>',
          title: 'title' + indexString
        })
      }
      data.push(options)
    }
  }

  $('.card-select').each(function (index) {
      $(this).select2({
          minimumResultsForSearch: -1,
          data: data[index],
          escapeMarkup: function (markup) {
              return markup;
          },
          templateResult: function (data) {
              return data.html;
          },
          templateSelection: function (data) {
              return data.text;
          }
      });
  });

  $('.js_medical-index-popup_open').on('click', function() {
    $('.js_medical-index-popup').addClass('open')
  })
  $('.js_medical-index-popup_close').on('click', function() {
    $('.js_medical-index-popup').removeClass('open')
  })

  $('.js_profile-open').on('click', function() {
    $('.js_toggle-show-profile-date').toggleClass('is-open')
  });

  function mobileNav() {
    $('.burger').on('click', function () {
      if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $('.navigation').removeClass('open');
        $('body').removeClass('ov-hidden');
      } else {
        $(this).addClass('open');
        $('.navigation').addClass('open');
        $('body').addClass('ov-hidden');
      }
    });
  }
  mobileNav();

  $('.main-icon').on('click', function(event) {
    event.preventDefault();
    const sc = $(this).attr("href");
    const dn = $(sc).offset().top;
    $('html, body').animate({scrollTop: dn}, 500);
  });

  const heightText = $('.card-descriptions__text-shell').height();
  const CONST_HEIGHT = 80;
  const more = $('.card-descriptions__more');

  $('.card-actions .see-more').each(function (e) {
    $(this).on('click', function (e) {
      e.preventDefault();

      if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $(this).text('See More Info');
        $(this).closest('.card').removeClass('open');

        // $('.card-descriptions__text').css('max-height', '100%');
        // $('.card-descriptions__more').removeClass('show');

        $(this).closest('.card').find('.card-descriptions-wrapper').removeClass('card-descriptions-wrapper--show');
        $(this).closest('.card').find('.lists').removeClass('lists--show');
      } else {
        // delete for all
        const lessBtn = $('.card-actions .see-more');
        lessBtn.text('See More Info');
        lessBtn.removeClass('open');
        $('.card').removeClass('open');
        $('.card-descriptions-wrapper').removeClass('card-descriptions-wrapper--show');
        $('.lists').removeClass('lists--show');
        $('.card-descriptions__more').removeClass('show');

        // $('.card-descriptions__text').css('max-height', '80px');
        // if ($(this).closest('.card').find('.card-descriptions__text-shell').height() > CONST_HEIGHT) {
        //   $('.card-descriptions__more').addClass('show');
        // }

        // delete for current
        $(this).addClass('open');
        $(this).text('See Less Info');
        $(this).closest('.card').addClass('open');
        $(this).closest('.card').find('.card-descriptions-wrapper').addClass('card-descriptions-wrapper--show');
        $(this).closest('.card').find('.lists').addClass('lists--show');
      }
    })
    
  });

  more.on('click', function () {
    $(this).prev('.card-descriptions__text').css('max-height', '100%');
    $('.card-descriptions__more').removeClass('show');
  });

  $('.js_medical-form__accordion-item').on('click', function() {
    $(this).toggleClass('active')
  })

  $('.js_header-search_open').on('click', function(e) {
    e.preventDefault;
    $('.js_header-search-block').addClass('opened')
  })
  $('.js_header-search_close').on('click', function() {
    $('.js_header-search-block').removeClass('opened')
  })

  function checkCookie() {
    const cookie = document.cookie.split('; ');
    const existCookie = cookie.some(el => el === 'legalAdultAge=true')
    if(!existCookie) {
      $('.legal-adult-age').addClass('show')
    }
  }
  checkCookie()

  $('.js_legal-adult-age_agree').on('click', function() {
    document.cookie = "legalAdultAge=true; path = /;"
    $('.legal-adult-age').removeClass('show')
  })

  $('.js_legal-adult-age_close').on('click', function() {
    $('.js_legal-adult-age').removeClass('show')
    $('.js_modal-alert').addClass('show')
    $('body').css('overflow', 'hidden')
  })

  $('.js_profile__link').on('click', function(e) {
    e.preventDefault();
    const tabClass = '.' + $(this).attr('data-tab-class');
    $('.js_profile__link').removeClass('is-active')
    $('.profile__tab').removeClass('selected')
    $(this).addClass('is-active')
    $(tabClass).addClass('selected')
  })
});
